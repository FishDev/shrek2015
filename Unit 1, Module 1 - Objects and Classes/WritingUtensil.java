abstract class WritingUtensil {
	String color = "blue";
	String brand;
	
	abstract void setColor(String newColor);
	abstract String getColor();
	
	abstract void setBrand(String newBrand);
	abstract String getBrand();
}

interface RubberGrip {
	void takeOffToClean();

	
}

interface SecretSpyTool{
	String record();
	void shootLaser();
	void makeNoise();
	void exposeKnifeEdge();
	void activateToothpickMode();
}

class Pen extends WritingUtensil {
	private int amountOfInk = 5;
	
	void setColor(String newColor) {
		color = newColor;
	}
	String getColor() {
		return color;
	}
	
	void setBrand(String newBrand) {
		brand = newBrand;
	}
	String getBrand() {
		return brand;
	}
	
	public int getInk() {
		return amountOfInk;
	}
}

class RubberGripPen extends Pen implements RubberGrip {

	@Override
	public void takeOffToClean() {
		// TODO Auto-generated method stub
		System.out.println("Clean me");
	}
	
}

class SecretSpyRubberGripPen extends RubberGripPen implements SecretSpyTool {

	@Override
	public String record() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void shootLaser() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void makeNoise() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exposeKnifeEdge() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activateToothpickMode() {
		// TODO Auto-generated method stub
		
	}
	
}