import static java.lang.System.out;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class BinarySearch {
	private static Scanner scanner;
	public static boolean result = false;
	
	public static void searchArray(int[] array, int value) {
		if(array.length!=1) {
			if(array[array.length/2]==value) {
				result = true;
			} else {
				if(array[array.length/2]>value) {
					searchArray(Arrays.copyOfRange(array, 0, (array.length/2)-1), value);
				} else if(array[array.length/2]<value) {
					searchArray(Arrays.copyOfRange(array, (array.length/2)+1, array.length-1), value);
				}
			}
		} else {
			if(array[0]==value) {
				result = true;
			}
		}
	}
	
	public static void sortArray(int[] array, int a, int b){
	    if(b<=a || a>=b) {
	    	
	    } else { 
	        int x = array[a]; 
	        int i = a+1;
	        int c;
	        
	        for(int j=a+1; j<=b; j++){
	            if(x>array[j]){
	                c = array[j]; 
	                array[j] = array[i]; 
	                array[i] = c; 
	                i++; 
	            }
	        }

	        array[a] = array[i-1]; 
	        array[i-1] = x; 
	        
	        sortArray(array, a, i-2); 
	        sortArray(array, i, b); 
	    }
	}
	
	public static void printArray(int[] array) {
		out.print("[");
		for(int i=0; i<array.length; i++) {
			if(i==0) {
				out.print(array[i]);
			} else {
				out.print(" ");
				out.print(array[i]);
			}
			if(i==(array.length-1)) {
				;
			} else {
				out.print(",");
			}
		}
		out.println("]");
	}
	
	public static void main(String args[]) {
		Random random = new Random();
		int[] array = new int[random.nextInt(1000) + 5];
		for(int i=0; i<array.length; i++) {
			array[i] = random.nextInt(10000) + 1;
		}
		printArray(array);
		
		scanner = new Scanner(System.in);
		out.print("Integer to search for: ");
		int value = scanner.nextInt();
		
		sortArray(array, 0, array.length-1);
		searchArray(array, value);
		out.print(result);
		out.println();
	}
}
