import static java.lang.System.out;
import java.util.Random;
import java.util.Scanner;

public class BasicSearch {
	private static Scanner scanner;
	public static boolean result = false;
	public static int location = 0;

	public static void searchArray(int[] array, int value) {
		for(int i=0; i<array.length; i++) {
			if(array[i]==value) {
				result = true;
				location = i;
				break;
			}
		}
	}
	
	public static void printArray(int[] array) {
		out.print("[");
		for(int i=0; i<array.length; i++) {
			if(i==0) {
				out.print(array[i]);
			} else {
				out.print(" ");
				out.print(array[i]);
			}
			if(i==(array.length-1)) {
				;
			} else {
				out.print(",");
			}
		}
		out.println("]");
	}
	
	public static void main(String args[]) {
		Random random = new Random();
		int[] array = new int[random.nextInt(1000) + 5];
		for(int i=0; i<array.length; i++) {
			array[i] = random.nextInt(10000) + 1;
		}
		printArray(array);
		
		scanner = new Scanner(System.in);
		out.print("Integer to search for: ");
		int value = scanner.nextInt();
		
		searchArray(array, value);
		out.print(result);
		if(result==true) {
			out.print(" at " + location);
		}
		out.println();
	}
}
